package com.example.ali.top10;

/**
 * Created by ali on 8/18/17.
 * <p>
 * ma in class ro sakhtim chon vaghti mikhayem dar yek listview neshoon bedim yek arraye az ye
 * chizi mikhayem hala chon ma darim data ro az net migim bayad hame oon haro dar yek ja zakhire konim
 * pas in class ro sakhtim ta dar parsexml az oon yek arrayelist besazim va oonja har chi parse
 * mikonim be in field ha nesbat midim
 */

public class RssfeedHolder {

    private String name;
    private String artist;
    private String summary;
    private String imageUrl;
    private String releaseDate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {//hame object ha vaghti beshoon migi .toString() age methode toString() barashoon ovverride shode bashe oon ro neshoon mide age nashode bashe esme khode classesh ro mide
        //va base listView ham age az listView sade estefade bokoni miyad az toString() estefade mikone ke age bashe in ro neshoon mide age nabashe hamoon esme class
        return "name = " + name + '\n' +
                ", artist = " + artist + '\n' +
                ", imageUrl = " + imageUrl + '\n' +
                ", summary = " + summary + '\n' +
                ", releaseDate = " + releaseDate + '\n';
    }
}
