package com.example.ali.top10;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private ListView listApp;
    private String feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
    private int feedLimit = 10;
    private String feedCashedUrl = "INVALIDATE";//vase inke ba url asli moghayese she age yeki bood dobare down nakone
    public static final String STATE_URL = "feedurl";
    public static final String STATE_LIMIT = "feedlimit";
    private boolean movieStateDisableTop = false;
    public static final String STATE_DISABLE_TOP_MOVIE = "movieStateDisableTop";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listApp = (ListView) findViewById(R.id.xmlListView);

        if (savedInstanceState != null) {//chon onrestore bade oncreate seda zade mishe va chon ma download ro dar oncreate anjam midim pas inja miyaem va bundle zakhre shode dar onsaveinstance ro migirim na dar onrestore
            feedLimit = savedInstanceState.getInt(STATE_LIMIT);
            feedUrl = savedInstanceState.getString(STATE_URL);
            movieStateDisableTop = savedInstanceState.getBoolean(STATE_DISABLE_TOP_MOVIE);
        }

        downlaodUrl(String.format(feedUrl, feedLimit));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_feeds, menu);
        if (feedLimit == 10) {
            menu.findItem(R.id.menu10).setChecked(true);
        } else {
            menu.findItem(R.id.menu25).setChecked(true);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        movieStateDisableTop = false;
        switch (id) {
            case R.id.menuFree:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topfreeapplications/limit=%d/xml";
//                movieStateDisableTop = false;
                break;
            case R.id.menuPaid:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/toppaidapplications/limit=%d/xml";
//                movieStateDisableTop = false;
                break;
            case R.id.menuSongs:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topsongs/limit=%d/xml";
//                movieStateDisableTop = false;
                break;
            case R.id.menuAlbum:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topalbums/limit=%d/xml";
                break;
            case R.id.menuMovie:
                feedUrl = "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topMovies/xml";
                movieStateDisableTop = true;
                break;
            case R.id.menu10:
            case R.id.menu25:
                if (!item.isChecked()) {
                    item.setChecked(true);
                    feedLimit = 35 - feedLimit;
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " stting feed limit to " + feedLimit);
                } else {
                    Log.d(TAG, "onOptionsItemSelected: " + item.getTitle() + " feedlimit unchanged");
                }
                break;
            case R.id.menuRefresh:
                feedCashedUrl = "INVALIDATE";
//                movieStateDisableTop = false;
                break;

            default:
                return super.onOptionsItemSelected(item);
        }
        if (id == R.id.menuMovie) {
            downlaodUrl(feedUrl);
        } else {
            downlaodUrl(String.format(feedUrl, feedLimit));
        }
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (movieStateDisableTop) {
            menu.findItem(R.id.menu10).setEnabled(false);
            menu.findItem(R.id.menu25).setEnabled(false);
        }
        invalidateOptionsMenu();
        return super.onPrepareOptionsMenu(menu);
    }


    private void downlaodUrl(String feedUrl) {
        if (!feedUrl.equalsIgnoreCase(feedCashedUrl)) {
            Log.d(TAG, "downlaodUrl: start async");
            DownloadData downloadData = new DownloadData();
            downloadData.execute(feedUrl);//in mishe voroodi doInBackground dar yek seprate thread
            feedCashedUrl = feedUrl;
            Log.d(TAG, "downlaodUrl: done");
        } else {
            Log.d(TAG, "downlaodUrl: url not change");
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(STATE_URL, feedUrl);
        outState.putInt(STATE_LIMIT, feedLimit);
        outState.putBoolean(STATE_DISABLE_TOP_MOVIE , movieStateDisableTop);
        super.onSaveInstanceState(outState);
    }

    private class DownloadData extends AsyncTask<String, Void, String> {
        private static final String TAG = "DownloadData";

        @Override
        protected String doInBackground(String... strings) {
            Log.d(TAG, "doInBackground: start with : " + strings[0]);
            String rssFeed = downloadXml(strings[0]);
            if (rssFeed == null) {
                Log.e(TAG, "doInBackground: Error");
            }

            return rssFeed;//inja age har chi joz string return kone error mide chon dar zamane tarif goftim ke khorooji string bashe
        }

        @Override
        protected void onPostExecute(String s) {//khorooji doInBackground mishe voroodi in tabe
            Log.d(TAG, "onPostExecute: the parameter is : " + s);
            super.onPostExecute(s);
            if (s != null) {
                ParseXml parseXml = new ParseXml();
                parseXml.parse(s);

                FeedAdapter adapter = new FeedAdapter(MainActivity.this, R.layout.list_record, parseXml.getApplication());
                listApp.setAdapter(adapter);
            } else {
                Toast.makeText(MainActivity.this, "Error in Downloading Data. Please Try Again", Toast.LENGTH_SHORT).show();
            }
        }

        private String downloadXml(String urlPath) {
            Log.d(TAG, "downloadXml: start with : " + urlPath);
            StringBuilder xmlResualt = new StringBuilder();

            try {
                URL url = new URL(urlPath);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                int responseCode = connection.getResponseCode();
                Log.d(TAG, "downloadXml: response code : " + responseCode);
//                InputStream inputStream = connection.getInputStream();
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                int charsRead;
                char[] inputBuffer = new char[500];
                while (true) {
                    charsRead = bufferedReader.read(inputBuffer);//mire az bufferedReader mikhoone va mirize dar inputBuffer va tedad char haye khoonde shode ro barmigardoone age be tahesh riside bashe mishe -1
                    if (charsRead < 0) {//be tahesh reside ba -1
                        break;
                    }
                    if (charsRead > 0) {
                        xmlResualt.append(String.copyValueOf(inputBuffer, 0, charsRead));
                    }//0 nemizarim chon bedin mani ast ke hanoo chizi vase down amade nashode
                }
                bufferedReader.close();//lazem nis ke hami ro Io ha ro close konim hamin bufferedReader ro close konim khodesh baghiye ro dorost mikone
                return xmlResualt.toString();//chon stringBuilder bood oon ro string kardam

            } catch (MalformedURLException error) {//MalformedURLException khodesh extend mikone az IOException vase hamin age ino payin tar az IOException benevisim hichvaght error url ro nemibinim
                Log.e(TAG, "downloadXml: invalid url " + error.getMessage());//for url exeption
            } catch (IOException error) {//for Io (inputStream and inputStreamReader)
                Log.e(TAG, "downloadXml: IO Exception reading data : " + error.getMessage());
            } catch (SecurityException error) {
                Log.e(TAG, "downloadXml: SecurityException need permision ? " + error.getMessage());
                error.printStackTrace();
            }

            return null;//har etefaghi ke baes she ke down nashe bayad null bargardoond
        }
    }
}
