package com.example.ali.top10;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by ali on 8/19/17.
 */

public class FeedAdapter extends ArrayAdapter {
    private static final String TAG = "FeedAdapter";

    private final int layoutResource;
    private final LayoutInflater layoutInflater;
    private List<RssfeedHolder> application;

    public FeedAdapter(@NonNull Context context, @LayoutRes int resource, List<RssfeedHolder> application) {//chera voroodi intori ast ? chon vase yek adapter ma be yek context va yek id layout va yek data niyaz darim
        super(context, resource);
        this.application = application;
        this.layoutInflater = LayoutInflater.from(context);//Returns LayoutInflater
        //or layoutInflater = (LayoutInflator) context.getSystemServices(Context.LYOUT_INFLATRO)
        //ma case inflatro yek context hatman niyaz darim
        this.layoutResource = resource;
    }

    @Override
    public int getCount() {
        return application.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {//vase darkhaste view jadid
        ViewHolder viewHolder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(layoutResource, parent, false);//in parent ro tahghihg kon
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        RssfeedHolder curentApp = application.get(position);//oon view ie ke gharar ast neshoon dade she ro migirim

        viewHolder.tvName.setText(curentApp.getName());
        viewHolder.tvArtist.setText(curentApp.getArtist());
        viewHolder.tvSummary.setText(curentApp.getSummary());

        return convertView;
    }

    private class ViewHolder {
        final TextView tvName;
        final TextView tvArtist;
        final TextView tvSummary;

        ViewHolder(View view) {
            this.tvName = (TextView) view.findViewById(R.id.tvName);
            this.tvArtist = (TextView) view.findViewById(R.id.tvArtist);
            this.tvSummary = (TextView) view.findViewById(R.id.tvSummary);
        }

    }
}
