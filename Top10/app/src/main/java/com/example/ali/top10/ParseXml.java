package com.example.ali.top10;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.StringReader;
import java.util.ArrayList;

import static org.xmlpull.v1.XmlPullParser.END_TAG;
import static org.xmlpull.v1.XmlPullParser.START_TAG;

/**
 * Created by ali on 8/18/17.
 */

public class ParseXml {
    private static final String TAG = "ParseXml";
    private ArrayList<RssfeedHolder> application;//inja yek arrayelist az class RssfeedHolder misazim ta har entry ro ke parse mikonim dar yeki az arraye ha zakhire konim va hamchenin be listview adapter ham bedimesh in arraye list ro

    public ParseXml() {
        this.application = new ArrayList<>();//dar inja besh migim ke application alan dg yek arraye list has va amade kar mishe
    }

    public ArrayList<RssfeedHolder> getApplication() {//in vase listView adapter estefade mishe ke zamani ke mikhayem besh yek list az data ha bedim
        return application;
    }

    public boolean parse(String xmlData) {//boolean has chon mikhayem age har etefaghi oftad ke natoones parse she false bargardoonim
        boolean status = true;//inja true mizarim va dar catch ha oon ro false mikonim ke neshoon mide natooneste parse kone
        boolean inEntry = false;//chon dar in xml do ta tage name darim yeki dar entry (ke niyazesh darim) va yeki biroon
        boolean gotImage100Height = false;//dar in xml 3 ta tage image has man mikham ooni ro begiram ke atribute height oon barabar ba 100 ast
        String textValue = "";//mishe oon text ie ke beyne do tag has
        RssfeedHolder currentRecord = null;//null mizarim chon age nazarim injoori dar switch case error mide ke in ro initial nakardim

        try {
            //khate payin ta khate setInput vase sakhte yek xml parser has va inke chetor in karo mikone be ma marboot nis
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();//ma nemitoonim haminjoori mostaghim az XmlPullParser yek instance new konim vase hamin avval bayad factory besazim
            factory.setNamespaceAware(true);
            XmlPullParser parser = factory.newPullParser();
            parser.setInput(new StringReader(xmlData));//StringReader : ba string mese yek stram barkhord mikone va oon ro mikhoone
            int eventType = parser.getEventType();//getEventType : dar vaghe be ma darbare vaziate tag ha mige ke aya be payan residim ya dar tag hastim ya dar text hastim va ba next() be jolo mire
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String tagName = parser.getName();//For START_TAG or END_TAG events, the (local) name of the current element is returned when namespaces are enabled. When namespace processing is disabled, the raw name is returned.
                //If the current event is not START_TAG, END_TAG, or ENTITY_REF, null is returned.
                switch (eventType) {
                    case START_TAG://dar tage shoroo bashe
                        Log.d(TAG, "parse: starts tag with : " + tagName);
                        if ("entry".equalsIgnoreCase(tagName)) {
                            inEntry = true;
                            currentRecord = new RssfeedHolder();
                        } else if ("image".equalsIgnoreCase(tagName) && inEntry) {
                            String imageResolution = parser.getAttributeValue(null, "height");//getAttributeValue : returns String value of attribute or null if attribute with given name does not exist
                            if (imageResolution != null) {
                                gotImage100Height = "100".equalsIgnoreCase(imageResolution);//in equalsIgnoreCase true ya false barmigardoone va mishe jaye 100 har height ie ke dar xml has ro nevesht masalan 53
                            }
                        }
                        break;

                    case XmlPullParser.TEXT://dar text bashe beyne do tag aghazi va payani
                        textValue = parser.getText();
                        break;

                    case END_TAG:// be tage payani residim yani <ali> in aghazi ast hala </ali> in payani ke in miyad in ro be ma mide
                        Log.d(TAG, "parse: ending tag with : " + tagName);
                        if (inEntry) {
                            if ("entry".equalsIgnoreCase(tagName)) {
                                application.add(currentRecord);
                                inEntry = false;
                            } else if ("name".equalsIgnoreCase(tagName))//chera naneveshtim : tagName.equalsIgnoreCase("entry") ? chon dar bala goftim ke tagName mitoone null bashe hala vaghti mese oon halate avvala
                                //minevisim oonvaght dar voroodi equalsIgnoreCase null gharar migire va kollan if mishe false va dg niyazi be null check nadarim vali oon joori mikhad null ro be yek chize gheyre null moghayese kone ke injoori bayad null check anjam bedim
                                currentRecord.setName(textValue);
                            else if ("artist".equalsIgnoreCase(tagName))
                                currentRecord.setArtist(textValue);
                            else if ("summary".equalsIgnoreCase(tagName))
                                currentRecord.setSummary(textValue);
                            else if ("releaseDate".equalsIgnoreCase(tagName))
                                currentRecord.setReleaseDate(textValue);
                            else if ("image".equalsIgnoreCase(tagName)) {
                                if (gotImage100Height) {
                                    currentRecord.setImageUrl(textValue);
                                }
                            }
                        }
                        break;

                    default:
                        //nothing else to do
                }
                eventType = parser.next();//mire be event badi dar vaghe mige ke hanooz dar xml becharkh ta be event badi beresi
            }

            //for testing
            for (RssfeedHolder app : application) {
                Log.d(TAG, "parse: *******************************************************************************************");
                Log.d(TAG, app.toString());//in toString ke goftim hala be kar miyad va oonjoori ke ma goftim namayesh dade mishe
            }


        } catch (Exception error) {//Exception : all the exception
            status = false;
            error.printStackTrace();
        }

        return status;
    }
}
